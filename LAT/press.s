set terminal pdf
set output "Press.pdf"
set grid
#set title 'PV'
#set key outside
#set nokey
set key at 1.2, 155.
set yrange [50:160]
set xlabel 'Time, s'
set ylabel 'Pressure, mmHg' 
plot 'HF_press' u 1:2 w l lw 3 t 'Heart failure',\
'Norm_press' u 1:2 w l lw 3 t 'Norm',\
'6k_press' u 1:2 w l lw 2 t '6000 rpm',\
'8k_press' u 1:2 w l lw 2 t '8000 rpm',\
'10k_press' u 1:2 w l lw 2 t '10000 rpm',\
'12k_press' u 1:2 w l lw 2 t '12000 rpm'
