set terminal pdf
set output "PV.pdf"
set grid
#set title 'PV'
#set key outside
#set nokey
set key at 200., 150.
set yrange [0:160]
set xlabel 'Volume, ml'
set ylabel 'Pressure, mmHg' 
plot 'HF_PV' u 1:2 w l lw 3 t 'Heart failure',\
'Norm_PV' u 1:2 w l lw 3 t 'Norm',\
'6k_PV' u 1:2 w l lw 2 t '6000 rpm',\
'8k_PV' u 1:2 w l lw 2 t '8000 rpm',\
'10k_PV' u 1:2 w l lw 2 t '10000 rpm',\
'12k_PV' u 1:2 w l lw 2 t '12000 rpm'
